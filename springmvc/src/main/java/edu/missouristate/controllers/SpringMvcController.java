package edu.missouristate.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SpringMvcController {

	/**
	 * Server is http://localhost:8080
	 * Get Mapping (Path) http://localhost:8080/example
	 * Php http://localhost:8080/example.php
	 * 
	 */
	@GetMapping(value="/example") 
	public String getIndexPage() {
		return "index.html";
	}
	
	@GetMapping(value="/examplezdfghdsgfh") 
	public String getIndexPage2() {
		return "index.html";
	}
		
}
